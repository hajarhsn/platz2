<?php
  namespace App\Http\Models;
  use Illuminate\Database\Eloquent\Model;

  class Ressource extends Model {
    /**
     * GETTER du user à qui appartient ce post.
     */
    public function categorieData (){
        return $this->belongsTo('App\Http\Models\Categorie');
    }
  }
