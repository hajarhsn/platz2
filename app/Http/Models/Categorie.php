<?php
  namespace App\Http\Models;
  use Illuminate\Database\Eloquent\Model;

  class Categorie extends Model {

    /**
     * GETTER des posts du user.
     */
    public function ressources() {
        return $this->hasMany('App\Http\Models\Ressource', 'categorie');
    }
  }
