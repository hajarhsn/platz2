<?php
 namespace App\Http\Controllers;

 use Illuminate\Support\Facades\View;
 use App\Http\Models\Ressource;

 class RessourcesController extends Controller {

   public function index(){
     return response()->json(Ressource::all());
   }
 }
