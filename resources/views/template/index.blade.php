<!DOCTYPE HTML>
<html lang="en">

<head>
  @include('template.partials._head')
</head>

  <body>

  <a name="ancre"></a>

  <!-- CACHE -->
  <div class="cache"></div>

  <!-- HEADER -->
  @include('template.partials._header')

  <!-- NAVBAR -->
  @include('template.partials._navigation')

  <!-- PORTFOLIO -->
	<div id="wrapper-container">
    <div class="container object">
      <div id="main-container-image">
        <div id="app">
          <router-view></router-view>
        </div>
      </div>
    </div>


  <!-- FOOTER -->
  	@include('template.partials._footer')
  </div>

  <!-- SCRIPT -->
   @include('template.partials._scripts')

  </body>
</html>
