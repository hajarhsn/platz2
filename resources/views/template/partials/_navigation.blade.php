{{--
  ./resources/views/template/partials/_navigation.blade.php
  Header
 --}}

 <div id="wrapper-navbar">
 	<div class="navbar object">
     <div id="wrapper-bouton-icon">
     	<div id="bouton-ai"><img src="img/icon-ai.svg" alt="illustrator" title="Illustrator" height="28" width="28"></div>
     	<div id="bouton-psd"><img src="img/icon-psd.svg" alt="photoshop" title="Photoshop" height="28" width="28"></div>
     	<div id="bouton-theme"><img src="img/icon-themes.svg" alt="theme" title="Theme" height="28" width="28"></div>
     	<div id="bouton-font"><img src="img/icon-font.svg" alt="font" title="Font" height="28" width="28"></div>
     	<div id="bouton-photo"><img src="img/icon-photo.svg" alt="photo" title="Photo" height="28" width="28"></div>
     	<div id="bouton-premium"><img src="img/icon-premium.svg" alt="premium" title="Premium" height="28" width="28"></div>
     </div>
   </div>
 </div>

 <!-- FILTER -->

 	<div id="main-container-menu" class="object">
   	<div class="container-menu">
     <div id="main-cross">
     	<div id="cross-menu"></div>
     </div>

     <div id="main-small-logo">
     	<div class="small-logo"></div>
     </div>

     <div id="main-premium-ressource">
      <div class="premium-ressource"><a href="#">Premium resources</a></div>
     </div>

     <div id="main-themes">
      <div class="themes"><a href="#">Latest themes</a></div>
     </div>

     <div id="main-psd">
      <div class="psd"><a href="#">PSD goodies</a></div>
     </div>

     <div id="main-ai">
      <div class="ai"><a href="#">Illustrator freebies</a></div>
     </div>

     <div id="main-font">
      <div class="font"><a href="#">Free fonts</a></div>
     </div>

     <div id="main-photo">
      <div class="photo"><a href="#">Free stock photos</a></div>
     </div>
    </div>
  </div>
